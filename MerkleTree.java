import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/*
MerkleTree
This class will construct a MerkleTree from a text file containing transaction ids.
It can produce a minimal set given a particular id for the instantiated tree, and verify the root of the tree given a minimal set.
 */

public class MerkleTree {

    private static ArrayList<Node> nodes = new ArrayList();
    private static ArrayList<String> leaftids;
    private static Node root;
    private static int treeDepth;
    private static int depthLevel;
    private static int index;

    private MerkleTree(ArrayList<String> basetids) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        buildTree(basetids);
    }

    private MerkleTree(ArrayList<String> basetids, String tid) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        buildTree(basetids);
        index = leaftids.indexOf("\"" + tid + "\"");
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
        String filetoString = readFile("txids.txt");
        filetoString.replaceAll("\n","");
        ArrayList<String> basetids = extractTids(filetoString);

        //Uncomment below to produce the MerkleTree and printed output alone:
            //MerkleTree mt = new MerkleTree(basetids);

        //Produce the MerkleTree and test the minimal-set-producing and root-proving methods given a certain transaction id:
        MerkleTree mt2 = new MerkleTree(basetids,"b5f60977102f95a9ed855b61acec86e2e434248b38c5f263ccf708a302832f3c");
        //MerkleTree mt2 = new MerkleTree(basetids,"e6922d44c520c52dca2cd5300784af55944c11839684e5c1671d9b330f871f55");

        //Print out the minimal set (starts with provided id, ends with root)
        ArrayList<String> minset = mt2.produceMinset(nodes.get(index));
        System.out.println("Minimal set: ");
        for (String s:minset){
            System.out.println(s);
        }
        System.out.println();
        System.out.println(mt2.matchesRoot(minset));
    }

    private void buildTree(ArrayList<String> hashes) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        leaftids = hashes;
        treeDepth = (int)(Math.ceil(Math.log(14)))+1;
        depthLevel = treeDepth;
        System.out.println("Size = " + hashes.size() + "\n");


        // Parents row is initialized with leafLevel results
        ArrayList<Node> parents = leafLevel(hashes);
        for (Node p: parents){
            nodes.add(p);
            p.depth = depthLevel;
            if (parents.indexOf(p) % 2 == 0){
                p.side = Node.Side.left;
            }
            else
                p.side = Node.Side.right;
        }


        // Parent node list is refreshed and resized with each depth level.
        while (parents.size() >= 2) {

            depthLevel--;
            System.out.println("Number of Branches at Depth " + depthLevel + " is " + parents.size() + "\n");

            for (int i=0; i<parents.size()-1; i+=2){
                System.out.println("Branch " + (i+1) + "= " + parents.get(i).toString());
                System.out.println("Branch " + (i+2) + "= " + parents.get(i+1).toString());
                System.out.println("Branch hash = " + computeParent(parents.get(i).toString(),parents.get(i+1).toString()) + "\n");
            }
            if (!(parents.size()%2==0)){
                System.out.println("Branch " + (parents.size()) + "= " + parents.get(parents.size()-1).toString());
                System.out.println("Branch " + (parents.size()+1) + "= " + parents.get(parents.size()-1).toString());
                System.out.println("Branch hash = " + computeParent(parents.get(parents.size()-1).toString(),parents.get(parents.size()-1).toString()) + "\n");
            }
            System.out.println("Completed Depth " + depthLevel);
            System.out.println("###########################################");

            System.out.println();
            parents = ascendLevel(parents);
            for (Node p: parents){
                nodes.add(p);
                p.depth = depthLevel;
                if (parents.indexOf(p)%2 ==0){
                    p.side = Node.Side.left;
                }
                else
                    p.side = Node.Side.right;
            }
        }

        // Now there is only one element in parents, root
        root = parents.get(0);
        depthLevel--;
        root.depth = depthLevel;
        System.out.println("FINAL MERKLE ROOT AT DEPTH = " + root.depth);
        System.out.println("Result: " + root + "\n");
    }


    // Instantiates node for each leaf
    private Node buildLeafNode(String id) {
        Node leaf = new Node();
        leaf.id = id.substring(1, id.length()-1);
        leaf.setDepth(depthLevel);
        nodes.add(leaf);
        return leaf;
    }

    // Instantiates parent node given two children
    private Node buildParentNode(Node leftChild, Node rightChild) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Node parent = new Node();
        leftChild.parent = parent;

        //If there is no right child, duplicate the left child
        if (rightChild == null) {
        parent.id = computeParent(leftChild.id, leftChild.id);
            parent.left = leftChild;
            parent.right = leftChild;
        }
        else {
            parent.id = computeParent(leftChild.id, rightChild.id);
            parent.left = leftChild;
            parent.right = rightChild;
        }

        return parent;
    }

    // Establishes the base leaf level of tree
    private ArrayList<Node> leafLevel(ArrayList<String> tids) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        ArrayList<Node> parentRow = new ArrayList(tids.size()/2);

        System.out.println("Number of Branches at Depth " + depthLevel + " is " + tids.size() + "\n");

        for (int i=0; i < tids.size()-1; i+=2) {
            Node leaf1 = buildLeafNode(tids.get(i));
            Node leaf2 = buildLeafNode(tids.get(i+1));
            Node parent = buildParentNode(leaf1, leaf2);

            leaf1.parent = parent;
            leaf2.parent = parent;

            System.out.println("Branch " + (i+1) + " = " + leaf1.toString());
            System.out.println("Branch " + (i+2) + " = " + leaf2.toString());
            System.out.println("Branch hash = " + parent.toString() + "\n");

            parentRow.add(parent);

        }
        System.out.println("Completed Depth " + depthLevel);
        System.out.println("###########################################");

        // If odd number of leafs, handle last entry
        if (!(tids.size() % 2 == 0)) {
            Node leaf = buildLeafNode(tids.get(tids.size() - 1));
            if (leaftids.indexOf(tids.get(tids.size() - 1))%2 == 0){
                leaf.side = Node.Side.left;
            }
            else
                leaf.side = Node.Side.right;
            Node parent = buildParentNode(leaf, null);
            parentRow.add(parent);
        }
        return parentRow;
    }

    // Builds parent level from lower level
    private ArrayList<Node> ascendLevel(ArrayList<Node> children) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        Node leftChild, rightChild;
        ArrayList<Node> parents = new ArrayList(children.size()/2);

        for (int i=0; i<children.size()-1; i+=2) {
            leftChild = children.get(i);
            rightChild = children.get(i+1);
            Node parent = buildParentNode(leftChild, rightChild);

            leftChild.parent = parent;
            rightChild.parent = parent;
            parents.add(parent);
        }

        // If odd number of children, pass in null as other child
        if (!(children.size()%2 == 0)) {
            Node child = children.get(children.size()-1);
            Node parent = buildParentNode(child, null);
            child.parent = parent;
            //System.out.println("This odd child, " + child.toString() + "'s parent = " + parent.toString());
            parents.add(parent);
        }
        return parents;
    }

    // Produces a minimum set (list) of ids, wherein the first element refers to the provided id, and the last element refers to the root.
    private ArrayList <String> produceMinset(Node node) throws NoSuchAlgorithmException {
        ArrayList<String> minset = new ArrayList<>(treeDepth);
        Node current = node;
        String tid = node.toString();
        minset.add(tid);
        int startIndex = nodes.indexOf(node);

            // If start leaf is a left-sided leaf, get the right-sided leaf
            if (startIndex % 2 == 0){
                //System.out.println("Yahoo 1: Started with a Left leaf! So continue with its right neighbor.");
                current = nodes.get(startIndex+1);
            }
            else {
                //System.out.println("Yahoo 2: Started with a Right leaf! So continue with its left neighbor.");
                current = nodes.get(startIndex-1);
            }

        // Add the sibling node
        minset.add(current.toString());

        // Use sidedness to determine path
        while (!(current.depth==2)) {

            if (current.parent.side==Node.Side.left){
                //System.out.println("Parent's a leftie");
                current = current.parent.parent.right;
                }

            else{
                //System.out.println("Parent's a rightie");
                current = current.parent.parent.left;
            }
            minset.add(current.toString());
        }

        if (current.depth == 2){
            current = current.parent;
//            System.out.println("Got the root now! " + current.toString());
            minset.add(current.toString());
        }

        return minset;
    }


    // Proves the root, given a minimum set of ids. Prints true if root produced from the minset matches the actual root
    private boolean matchesRoot(ArrayList<String> minset) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        System.out.println("Verification trail to root, given provided minimum set:");
        String par = "";

        // If the minset.get(0) corresponds to a left node, compute parent straight forward...
        if ((leaftids.indexOf("\"" + minset.get(0).toString() + "\"")) % 2 == 0) {
            //System.out.println("Computing in order of minset");
            par = computeParent(minset.get(0).toString(), minset.get(1).toString());
        } else {
            //System.out.println("Computing reverse order of minset");
            par = computeParent(minset.get(1).toString(), minset.get(0).toString());
        }
        System.out.println("Parent computed at depth level " + treeDepth + ": " + par);

        for (int i = 1; i < minset.size() - 2; i++) {
//            System.out.println("This is the i (next id in the minset list) I want to match: " + minset.get(i+1));
            for (int j = 0; j < nodes.size(); j++) {
                if (nodes.get(j).toString().equals(minset.get(i + 1))) {
//                    System.out.println("Got it at " + j);

                    // The order of input strings depends on both parity of j (position in the master list of nodes) and position relative to the duplicated node
                    if (!(j % 2 == 0)) {
                        if (j < 21) {
//                            System.out.println("It's a right child!");
//                            System.out.println("Computing par first: " + par);
//                            System.out.println("Computing get (i+1) next: " + minset.get(i + 1));
                            par = computeParent(par, minset.get(i + 1));
                        } else {
//                            System.out.println("It's a left child");
//                            System.out.println("Computing get (i+1) first: " + minset.get(i + 1));
//                            System.out.println("Computing par next: " + par);
                            par = computeParent(minset.get(i + 1), par);
                        }
                    } else {
                        if (j < 21) {
                            par = computeParent(minset.get(i + 1), par);
                        } else {
                            par = computeParent(par, minset.get(i + 1));
                        }
                    }
                }
            }
            System.out.println("Parent computed at depth level " + (treeDepth - i) + ": " + par);

        }
        return (par.equals(root.toString()));
    }

    // Standard parsing of a file into a String output
    private static String readFile(String path)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded);
    }

    // Turn the String into a list of transaction ids
    private static ArrayList<String> extractTids(String in){
        ArrayList<String> l = new ArrayList(Arrays.asList(in.split(",\n")));
        return l;
    }

    // Calculate the parent hash
    private String computeParent(String s1, String s2) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] inbytes1 = hexToBytes(s1);
        byte[] inbytes2 = hexToBytes(s2);

        byte[] revinbytes1 = reverseByteArray(inbytes1);
        byte[] revinbytes2 = reverseByteArray(inbytes2);

        //Concatenate the byte arrays
        byte[] combinedrevedBytes = new byte[revinbytes1.length + revinbytes2.length];
        System.arraycopy(revinbytes1, 0, combinedrevedBytes, 0, revinbytes1.length);
        System.arraycopy(revinbytes2, 0, combinedrevedBytes, revinbytes1.length,revinbytes2.length) ;

        byte[] twiceHashed = doublesha256(combinedrevedBytes);
        reverseByteArray(twiceHashed);
        return bytesToHex(twiceHashed);

    }

    // Converts bytes to hex string
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    // Converts hex string to bytes
    private byte[] hexToBytes(String s) {
        String news = null;
        if (s.startsWith("\"")){
            news = s.substring(1,s.length()-1);
        }
        else news = s;
        byte[] b = new byte[news.length()/2];
        for (int i = 0; i < b.length; i++) {
            int index = i*2;
            int value = Integer.parseInt(news.substring(index, index+2), 16);
            b[i] = (byte)value;
        }
        return b;
    }

    // Reverses the order of the bytes
    private byte[] reverseByteArray(byte[] inBytes) {
        int inLength = inBytes.length;
        for (int i = 0; i < (inLength >>1); i++) {
            byte temp = inBytes[i];
            inBytes[i] = inBytes[inLength-i-1];
            inBytes[inLength-i-1] = temp;
        }
        return inBytes;
    }

    // Perform SHA-256 twice
    private static byte[] doublesha256(byte[] b) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte firstHashed[] = md.digest(b);
        byte secondHashed[] = md.digest(firstHashed);
        return secondHashed;
    }
}
