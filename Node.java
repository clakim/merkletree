/*
Node
A Node object is created for each hash in the MerkleTree.
 */

public class Node {
    public String id;
    public Node left;
    public Node right;
    public Side side;
    public Node parent;
    public int depth;

    public enum Side {
        left, right
    }

    public int getDepth(){
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String toString() {
        return id;
    }
}