README.md
Lab 3, MPCS 56600 Blockchain Technology
Claire Kim

MerkleTree and helper class Node written in Java, located in src folder.

Run directions:
  javac *.java
  java MerkleTree

Notes:
As stands, java MerkleTree will construct the MerkleTree from txids.txt input and also yield output of minimal set given this tree and id ("b5f60977102f95a9ed855b61acec86e2e434248b38c5f263ccf708a302832f3c"), and verification of the root given the produced minimal set. Can tweak/uncomment to yield MerkleTree only or minimal set for another id.
